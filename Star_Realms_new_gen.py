###############################   STAR REALMS : Version hard   ######################################

import random as rdm
import matplotlib.pyplot as plt
import matplotlib.image as mp
import numpy as np
plt.rcParams['figure.dpi']=500

############## Effets des cartes ###############

def draw(n,joueur,deck,hand,discard): # la fonction pioche
    i = 0
    while len(deck[joueur]) >= 1 and i < n: # il reste des cartes a piocer et dans le deck
        k = rdm.randint(0,len(deck[joueur])-1)
        card_draw = deck[joueur][k]
        deck[joueur].pop(k)
        hand[joueur].append(card_draw)
        i = i+1
    if i< n: # plus de carte dans le deck
        if len(discard[joueur]) == 0: # evite une possible boucle infini
            return ()
        for j in range(len(discard[joueur])): # on transfert la defausse dans le deck
            deck[joueur].append(discard[joueur][-1])
            discard[joueur].pop()
        draw(n-i,joueur,deck,hand,discard) # on pioche les cartes qu'il nous reste

def heal(joueur,nbe,points):
    points[joueur] += nbe

def scrap_auto(joueur,discard):
    c,i = 0,0
    while c == 0 and i < len(discard[joueur]):
        if discard[joueur][i][0] <= 1: # on trouve un viper ou un scout
            discard[joueur].pop(i)
            c += 1
        i += 1

def scrap_player(joueur,discard):
    if len(discard[joueur]) > 0:
        affiche = []
        for carte in discard[joueur]:
            affiche.append(carte[5])
        affichage_image(affiche)
        affichage_achat(discard[joueur])
        print("======> Vous pouvez retirer une carte de votre défausse")
        k = int(input()) -1
        if k >= len(discard[joueur]):
            print("======> Cela n'est pas possible")
            scrap_player(joueur,discard)
            return ()
        if k == -1:
            return ()
        print("======> Vous avez retirez ",discard[joueur][k])
        discard[joueur].pop(k)
    else:
        print(" ( Scrap s'active dans le vide ) ")

def fill_shop(shop,cartes_A): # permet de remplir le magasin, tres pratique dans toute la suite
    while len(shop) < taille_shop and cartes_A != []:
        k = rdm.randint(0,len(cartes_A)-1)
        card_draw = cartes_A[k]
        shop.append(card_draw)
        cartes_A.pop(k)

def switch_auto(shop,cartes_A,c):
    for j in range(len(shop)):
        if shop[j][1] < c:
            shop.pop(j)
            fill_shop(shop,cartes_A)

def switch_player(shop,cartes_A,c):
    affiche = []
    for carte in shop:
        affiche.append(carte[5])
    affichage_image(affiche)
    affichage_achat(shop)
    print("======> Vous pouvez retirer une carte du magasin en sachant que vous avez",c,"crédits")
    k = int(input()) -1
    if k >= len(shop):
        print("======> Cela n'est pas possible")
        switch_player(shop,cartes_A,c)
        return()
    if k == -1:
        return ()
    print("======> Vous avez retirez ",shop[k])
    shop.pop(k)
    fill_shop(shop,cartes_A)
    
def discard_player(hand,discard,joueur):
    print("======> Le joueur",1-joueur,"doit défausser une carte")
    affiche = []
    for carte in hand[1 - joueur]:
        affiche.append(carte[5])
    affichage_image(affiche)
    affichage_reduit(hand[1-joueur])
    if len(hand[1 - joueur]) >0:
        k = int(input()) -1
        if k >= len(hand[1-joueur]) or k < 0:
            print("======> Et non, il va falloir le faire !")
            discard_player(hand,discard,joueur)
            return()
        print("======> Votre adversaire a défaussé",hand[1-joueur][k])
        discard[1-joueur].append(hand[1-joueur][k])
        hand[1-joueur].pop(k)

def more_damages(nbe,points,point_tour,joueur):
    points[1 - joueur] += - nbe
    point_tour[0] += nbe


def destroy_base(base,joueur,discard):
    print("=======> Vous pouvez détruire une base")
    if base[1 - joueur] != []:
        affichage_image_bases(base[1-joueur])
        affichage_achat(base[1-joueur])
        k = int(input()) -1
        if k >= len(base[1-joueur]) or k < -1:
            print("======> Cela n'est pas possible")
            destroy_base(base,joueur,discard)
            return ()
        if k >= 0:
            print("=====> Vous avez détruit",bases[1 - joueur][k][0])
            discard.append(bases[1 - joueur][k][0])
            bases[1 - joueur].pop(k)


############## Initialisation du paquet de carte #################

cartes = [[0,0,1,"","Scout",[],[],[],["ship"]],[0,1,0,"","Viper",[],[],[],["ship"]]]
paquet = [[1,0,2,"blue","Federation Shuttle",[],[["heal",4]],[],["ship"]],[1,0,2,"blue","Federation Shuttle",[],[["heal",4]],[],["ship"]],[1,0,2,"blue","Federation Shuttle",[],[["heal",4]],[],["ship"]]
          ,[2,0,2,"blue","Cutter",[["heal",4]],[["damages",4]],[],["ship"]],[2,0,2,"blue","Cutter",[["heal",4]],[["damages",4]],[],["ship"]],[2,0,2,"blue","Cutter",[["heal",4]],[["damages",4]],[],["ship"]]
          ,[2,0,0,"blue","Storage Silo",[["heal",2]],[["more_gold",2]],[],["base",3]],[2,0,0,"blue","Storage Silo",[["heal",2]],[["more_gold",2]],[],["base",3]]
          ,[3,3,2,"blue","Patrol Cutter",[],[["heal",4]],[],["ship"]],[3,3,2,"blue","Patrol Cutter",[],[["heal",4]],[],["ship"]]
          ,[4,0,4,"blue","Freighter",[],[],[],["ship"]],[4,0,4,"blue","Freighter",[],[],[],["ship"]]
          ,[5,4,0,"blue","Trade Escort",[["heal",4]],[["draw"]],[],["ship"]]
          ,[6,5,0,"blue","Flagship",[["Draw"]],[["heal",5]],[],["ship"]]
          ,[7,0,2,"blue","Central Office",[],[["draw"]],[],["base",6]]
          ,[8,5,0,"blue","Command Ship",[["heal",4],["draw"],["draw"]],["destroy"],[],["ship"]]
          ,[3,0,1,"blue","Trading Post",[["heal",1]],[],[["damages",3]],["base",4]],[3,0,1,"blue","Trading Post",[["heal",1]],[],[["damages",3]],["base",4]]
          ,[4,0,2,"blue","Barter World",[["heal",2]],[],[["damages",5]],["base",4]],[4,0,2,"blue","Barter World",[["heal",2]],[],[["damages",5]],["base",4]]
          ,[5,3,0,"yellow","War World",[],[["damages",4]],[],["base",4]]
          ,[3,0,1,"yellow","Survey Ship",[["draw"]],[],[["discard"]],["ship"]],[3,0,1,"yellow","Survey Ship",[["draw"]],[],[["discard"]],["ship"]]
          ,[3,4,0,"yellow","Imperial Frigate",["discard"],[["damages",2]],[["draw"]],["ship"]],[3,4,0,"yellow","Imperial Frigate",["discard"],[["damages",2]],[["draw"]],["ship"]]
          ,[4,0,0,"yellow","Space Station",[["discard"]],[["damages",2]],[["more_gold",4]],["base",4]],[4,0,0,"yellow","Space Station",[["discard"]],[["damages",2]],[["more_gold",4]],["base",4]]
          ,[6,5,0,"yellow","Battlecruiser",[["draw"]],[["discard"]],[["draw"],["destroy"]],["ship"]]
          ,[1,2,0,"yellow","Imperial Fighter",[["discard"]],[["damages",2]],[],["ship"]],[1,2,0,"yellow","Imperial Fighter",[["discard"]],[["damages",2]],[],["ship"]],[1,2,0,"yellow","Imperial Fighter",[["discard"]],[["damages",2]],[],["ship"]]
          ,[7,7,0,"yellow","Dreadnaught",[["draw"]],[],[["damages",5]],["sh1ip"]]
          ,[5,5,0,"yellow","Aging Battleship",[],[["draw"]],[["damages",2],["draw"],["draw"]],["ship"]]
          ,[1,2,0,"red","Battle Bot",[["scrap"]],[["damages",2]],[],["ship"]]
          ,[4,5,0,"green","Battle Screecher",[["switch"],["switch"],["switch"],["switch"],["switch"]],[["more_gold",2]],[],["ship"]],[4,5,0,"green","Battle Screecher",[["switch"],["switch"],["switch"],["switch"],["switch"]],[["more_gold",2]],[],["ship"]]
          ,[3,0,0,"red","Battle Station",[],[],[["damages",5]],["base",5]],[3,0,0,"red","Battle Station",[],[],[["damages",5]],["base",5]]
          ,[1,3,0,"green","Blob Fighter",[],[["draw"]],[],["ship"]],[1,3,0,"green","Blob Fighter",[],[["draw"]],[],["ship"]],[1,3,0,"green","Blob Fighter",[],[["draw"]],[],["ship"]]
          ,[8,0,0,"blue","Capitol World",[["heal",6],["draw"]],[],[],["base",6]]
          ,[1,0,0,"yellow","Cargo Launch",[["draw"]],[],[["more_gold",1]],["ship"]],[1,0,0,"yellow","Cargo Launch",[["draw"]],[],[["more_gold",1]],["ship"]],[1,0,0,"yellow","Cargo Launch",[["draw"]],[],[["more_gold",1]],["ship"]]
#          ,[3,0,3,"green","Cargo Pod",[],[["damages",3]],[["damages",3]],["ship"]]
          ,[4,0,3,"red","Mining Mech",[["scrap"]],[["damages",3]],[],["ship"]],[4,0,3,"red","Mining Mech",[["scrap"]],[["damages",3]],[],["ship"]]
          ,[3,2,0,"yellow","Falcon",[["draw"]],[],[["discard"]],["ship"]],[3,2,0,"yellow","Falcon",[["draw"]],[],[["discard"]],["ship"]]
          ,[4,0,3,"blue","Fronteir Ferry",[["heal",4]],[],[["destroy"]],["ship"]],[4,0,3,"blue","Fronteir Ferry",[["heal",4]],[],[["destroy"]],["ship"]]
#          ,[6,2,2,"red","Frontier Station",[],[],[],["base",6]]
          ,[4,5,0,"yellow","Gunship",[["discard"]],[],[["more_gold",4]],["ship"]],[4,5,0,"yellow","Gunship",[["discard"]],[],[["more_gold",4]],["ship"]],[4,5,0,"yellow","Gunship",[["discard"]],[],[["more_gold",4]],["ship"]]
          ,[5,4,0,"yellow","Heavy Cruiser",[["draw"]],[["draw"]],[],["ship"]]
          ,[7,0,0,"yellow","Imperial Palace",[["draw"],["discard"]],[["damages",4]],[],["base",6]]
          ,[5,0,3,"yellow","Imperial Trader",[["draw"]],[["damages",4]],[],["ship"]]
          ,[7,3,3,"blue","Loyal Colony",[["heal",3]],[],[],["base",6]]
          ,[5,6,0,"red","Mech Cruiser",[["scrap"]],[["destroy"]],[],["ship"]]
          ,[6,6,0,"blue","Peacekeeper",[["heal",6]],[["draw"]],[],["ship"]]
          ,[6,0,3,"blue","Port of Cail",[],[],[["draw"],["destroy"]],["base",6]]
          ,[2,4,0,"green","Predator",[],[["draw"]],[],["ship"]],[2,4,0,"green","Predator",[],[["draw"]],[],["ship"]],[2,4,0,"green","Predator",[],[["draw"]],[],["ship"]]
#          ,[3,6,0,"green","Ravager",[["switch"],["switch"]],[],[],["ship"]]
#          ,[2,0,2,"red","Repair Bot",[["scrap"]],[],[["damages",2]],["ship"]]
          ,[4,4,0,"blue","Security Craft",[["heal",3]],[["more_gold",3]],[],["ship"]],[4,4,0,"blue","Security Craft",[["heal",3]],[["more_gold",3]],[],["ship"]]
#          ,[1,0,2,"yellow","Star Barge",[],[["discard"],["damages",2]],["ship"]]
          ,[7,5,0,"red","The Ark",[["scrap"],["scrap"],["draw"],["draw"]],[],[["destroy"]],["ship"]]
          ,[4,0,0,"red","The Oracle",[["scrap"]],[["damages"]],[],["base",5]]
#          ,[2,0,3,"blue","Trade Hauler",[["heal",3]],[],[],["ship"]]
#          ,[1,0,1,"blue","Trade Raft",[],[["draw"]],[["more_gold",1]],["ship"]]
#          ,[3,0,1,"green","Trade Wheel",[],[["damages",2]],[],["base",5]]
          ,[2,1,0,"yellow","Corvette",[["draw"]],[["damages",2]],[],["ship"]],[2,1,0,"yellow","Corvette",[["draw"]],[["damages",2]],[],["ship"]],[2,1,0,"yellow","Corvette",[["draw"]],[["damages",2]],[],["ship"]]
          ,[1,0,1,"red","Trade Bot",[["scrap"]],[["damages",2]],[],["ship"]],[1,0,1,"red","Trade Bot",[["scrap"]],[["damages",2]],[],["ship"]],[1,0,1,"red","Trade Bot",[["scrap"]],[["damages",2]],[],["ship"]]
          ,[3,0,2,"red","Supply Bot",[["scrap"]],[["damages",2]],[],["ship"]],[3,0,2,"red","Supply Bot",[["scrap"]],[["damages",2]],[],["ship"]]
          ,[4,5,0,"red","Patrol Mech",[],[["scrap"]],[],["ship"]],[4,5,0,"red","Patrol Mech",[],[["scrap"]],[],["ship"]]
          ,[4,2,0,"red","Border Fort",[],[["scrap"]],[],["base",5]],[4,2,0,"red","Border Fort",[],[["scrap"]],[],["base",5]]
          ,[6,6,0,"red","Missile Mech",["destroy"],[["draw"]],[],["ship"]]
          ,[2,2,0,"red","Missile Bot",[["scrap"]],[["damages",2]],[],["ship"]],[2,2,0,"red","Missile Bot",[["scrap"]],[["damages",2]],[],["ship"]],[2,2,0,"red","Missile Bot",[["scrap"]],[["damages",2]],[],["ship"]]
          ,[5,4,0,"red","Battle Mech",[["scrap"]],[["draw"]],[],["ship"]]
          ,[8,0,0,"red","Brain World",[["scrap"],["scrap"],["draw"],["draw"]],[],[],["base",6]]
          ,[7,6,0,"red","Machine Base",[["scrap"]],[],[],["base",6]]
          ,[3,4,0,"red","Convoy Bot",[],[["scrap"]],[],["ship"]],[3,4,0,"red","Convoy Bot",[],[["scrap"]],[],["ship"]]
          ,[6,8,0,"green","Battle Blob",[],[["draw"]],["damages",4],["ship"]]
          ,[2,2,0,"red","Patrol Bot",[],[["scarp"]],[],["ship"]],[2,2,0,"red","Patrol Bot",[],[["scarp"]],[],["ship"]]
          ,[3,0,0,"yellow","Fighter Base",[],[["discard"]],[],["base",5]],[3,0,0,"yellow","Fighter Base",[],[["discard"]],[],["base",5]]
          ,[4,6,0,"green","Blob Destroyer",[],[["switch"],["destroy"]],[],["ship"]],[4,6,0,"green","Blob Destroyer",[],[["switch"],["destroy"]],[],["ship"]]
          ,[4,3,0,"green","Bioformer",[],[],[["more_gold",3]],["base",4]],[4,3,0,"green","Bioformer",[],[],[["more_gold",3]],["base",4]]
          ,[3,1,0,"green","Blob Wheel",[],[],[["more_gold",3]],["base",5]],[3,1,0,"green","Blob Wheel",[],[],[["more_gold",3]],["base",5]]
          ,[1,3,0,"green","SpikePod",[["switch"],["switch"]],[],[["damages",2]],["ship"]],[1,3,0,"green","SpikePod",[["switch"],["switch"]],[],[["damages",2]],["ship"]],[1,3,0,"green","SpikePod",[["switch"],["switch"]],[],[],["ship"]]
          ,[7,6,0,"green","Mothership",[["draw"]],[["draw"]],[],["ship"]]
          ,[8,0,0,"green","Blob World",[],[["draw"],["draw"],["draw"]],[],["base",6]]
          ,[3,5,1,"green","Ram",[],[["damages",2]],[["more_gold",2]],["ship"]],[3,5,1,"green","Ram",[],[["damages",2]],[["more_gold",2]],["ship"]]
          ,[2,0,3,"green","Trade Pod",[],[["damages",2]],[],["ship"]],[2,0,3,"green","Trade Pod",[],[["damages",2]],[],["ship"]],[2,0,3,"green","Trade Pod",[],[["damages",2]],[],["ship"]]
          ,[2,4,0,"green","Battle Pod",[["switch"]],[["damages",2]],[],["ship"]],[2,4,0,"green","Battle Pod",[["switch"]],[["damages",2]],[],["ship"]]
          ,[5,3,0,"green","The Hive",[],[["draw"]],[],["base",5]]]

nbe_prospecteur = 10
cartes_par_couleur = 11

for i in range(1,nbe_prospecteur):    # ajout des prospecteurs
    cartes.append([2,0,2,"","Prospector",[],[],[["damages",2]],["ship"]])

for ext in paquet:
    cartes.append(ext)

for i in range(len(cartes)):        # ajout d'un identifiant a chaque carte
    cartes[i] = [i] + cartes[i]

# for carte in cartes:
#     img = mp.imread(carte[5] + ".jpg")
#     if (len(img),len(img[0])) != (300,214):
#         print(len(img),len(img[0]))

############## Fonction de jeu ###############"

#Parametres a initialiser

cartes_A = cartes[2+nbe_prospecteur:]
shop = []
taille_shop = 5
deck = [[],[]] # une case pour chaque joueur
discard = [[],[]]
hand = [[],[]]
ide_prospecteur = [2]
points = [50,50]
gain = []
retour = [[50],[50]]
bases = [[],[]]

####### Les fontions Fill_ #######


def fill_deck(deck):  # ne s'effectue qu'une fois au debut de la partie
    for k in range(2):
        for i in range(8):
            deck[k].append(cartes[0])
        for j in range(2):
            deck[k].append(cartes[1])
###

fill_shop(shop,cartes_A)
fill_deck(deck)
##### Une fonction affichage qui epure le format des cartes ######


def affichage(liste):
    res = []
    for i in range(len(liste)):
        res.append((liste[i][5],liste[i][1])) # le nom, le cout
    print("<<<<<<<<<<<<<<<<<<  ",res,"   >>>>>>>>>>>>>>>>>>")


def affichage_image(liste):
    image = np.zeros((300,214*len(liste),3),dtype=np.uint8)
    c = 0
    for cartes in liste:
        dessin = mp.imread(cartes+ ".jpg")
        for i in range(300):
            for j in range(214):
                image[i,214*c+j] = dessin[i,j]
        c += 1
    plt.imshow(image,interpolation = 'nearest')
#    image.set_cmap('hot')
    plt.axis('off')
    plt.show()

def affichage_image_bases(liste):
    image = np.zeros((214,300*len(liste),3),dtype=np.uint8)
    c = 0
    for cartes in liste:
        dessin = mp.imread(cartes+ ".jpg")
        for i in range(300):
            for j in range(214):
                image[213-j,300*c+i] = dessin[i,j]
        c += 1
    plt.imshow(image,interpolation = 'nearest')
#    image.set_cmap('hot')
    plt.axis('off')
    plt.show()

def affichage_achat(liste):
    prop = 1
    for carte in liste:
        print(prop,"----> ",carte)
        prop += 1
    print(0,"---->  Ne rien faire")

def affichage_reduit(liste):
    prop = 1
    for carte in liste:
        print(prop,"----> ",carte[1:])
        prop += 1

def affichage_base(liste):
    prop = 1
    for carte in liste:
        print(prop,"----> ",carte[0][1:])
        prop +=1
    print(0,"----> Tout dans la tête !")

##### Une fonction qui reconnait la couleur des cartes joue dans le tour #####


def empreinte_couleur(ensemble): # a coder mieux si possible
    if ensemble == "":
        return -1
    elif ensemble == "blue":
        return 0
    elif ensemble == "yellow":
        return 1
    elif ensemble == "red":
        return 2
    else:
        return 3

def repart_points(points,joueur,bases,discard,degats):
    destructible = []
    affiche = []
    for i in range(len(bases[1-joueur])): # on parcours les bases adverses
        if degats >= bases[1 - joueur][i][1]:
            affiche.append(bases[1-joueur][i][0][5])
            destructible.append([bases[1-joueur][i][0],i]) # on récupére les bases a porté de dêgats
    if destructible != []:
        print()
        print("=====> Vous pouvez détruire :")
        affichage_image_bases(affiche)
        affichage_base(destructible)
        print("=====> En sachant que vous avez ",degats," dêgats")
        print()
        k = int(input()) -1
        if k >= len(destructible):
            print("======> Cela n'est pas possible")
            repart_points(points,joueur,bases,discard,degats)
            return ()
        if k >= 0:
            choix = destructible[k][1]
            cout = bases[1 - joueur][choix][1]
            print("=====> Vous avez détruit",bases[1 - joueur][choix][0])
            discard.append(bases[1 - joueur][choix][0])
            bases[1 - joueur].pop(choix)
            repart_points(points,joueur,bases,discard,degats - cout)
        if k == -1:
            points[1 - joueur] += - degats
    else:
        points[1 - joueur] += - degats

def buy(card,joueur,cartes_A,discard,shop): # on lui donne la carte a achete dans le magasin
    t = shop.pop(card)
    discard[joueur].append(t)
    fill_shop(shop,cartes_A)


def buy_player(joueur,c,cartes_A,discard,shop,ide_prospecteur):
    possible = []
    affiche1 = []
    affiche2 = []
    for i in range(len(shop)):
        [ide,a,p,cf,e,n,effet,effet_plus,scr,t] = shop[i]
        if a <= c:
            affiche2.append(n)
            affiche1.append((n,a,p,cf,e,effet,effet_plus,t))
            possible.append((i,a,ide))
    if c >= 2 and ide_prospecteur[0] < 2 + nbe_prospecteur:
        affiche2.append("Prospector")
        affiche1.append(("prospector",2,0,2,"",[],[],["ship"]))
        possible.append((-1,2,ide_prospecteur[0]))
    if possible == []:
        return ()
    affichage(shop)
    print()
    print("==========================>   Vous pouvez acheter :")
    print()
    affichage_image(affiche2)
    affichage_achat(affiche1)
    print()
    print("==========================>   en sachant que vous avez ",c," crédits")
    print("======> Que voulez vous acheter ?")
    k = int(input()) -1
    if k >= len(possible):
        print("======> Cela n'est pas possible")
        buy_player(joueur,c,cartes_A,discard,shop,ide_prospecteur)
        return ()
    if k != -1:
        (j,a_final,ide) = possible[k]
        if j >= 0:
            buy(j,joueur,cartes_A,discard,shop)
            print(" ( Joueur",joueur,"a acheté ",affiche2[k],")")
            return buy_player(joueur,c - a_final,cartes_A,discard,shop,ide_prospecteur)
        else:
            discard[joueur].append(cartes[ide_prospecteur[0]])
            ide_prospecteur[0] += 1
            return buy_player(joueur,c - 2,cartes_A,discard,shop,ide_prospecteur)
    return ()

def featch(played,ide,bases,joueur):
    i,c,mp,mb = 0,False,len(played),len(bases[joueur])
    while not c:
        if i < mp and played[i][0] == ide:
            played.pop(i)
            c = True
        if i >= mp and i - mp < mb and bases[joueur][i-mp][0][0] == ide:
            bases[joueur].pop(i-mp)
            c = True
        i += 1
        if i == mb+mp: # au cas où
            c = True


def boucle(joueur,deck,hand,discard,cartes_A,points,shop,ide_prospecteur,param):
    [credits_tot,couleur,point_tour,futur_switch,played] = param
    effet_scrap = []
    while hand[joueur] != []:
        [ide,a,p,c,e,n] = hand[joueur][0][:6]
        effet = []
        for ef in hand[joueur][0][6]:
            effet.append(ef)
        allies = empreinte_couleur(e)
        if hand[joueur][0][8] != []:
            effet_scrap.append([hand[joueur][0][8],ide,n])
        if allies >= 0 and couleur[allies][1] > 0:
            if couleur[allies][1] ==1:
                for ef_c in couleur[allies][0]:
                    effet.append(ef_c)
            else:
                if len(hand[joueur][0]) > 7:
                    for ef_c in hand[joueur][0][7]:
                        effet.append(ef_c)
        while effet != []:
            if effet[-1][0] == "draw":
                draw(1,joueur,deck,hand,discard)
            if effet[-1][0] == "heal":
                heal(joueur,effet[0][1],points)
            if effet[-1][0] == "scrap":
                print("############################################################### Scrap ###############################################################")
                scrap_player(joueur,discard)
            if effet[-1][0] == "switch":
                futur_switch += 1
            if effet[-1][0] == "discard":
                print("############################################################### Discard ###############################################################")
                discard_player(hand,discard,joueur)
            if effet[-1][0] == "damages":
                more_damages(effet[-1][1],points,point_tour,joueur)
            if effet[-1][0] == "more_gold":
                credits_tot += effet[-1][1]
            if effet[-1][0] == "destroy":
                destroy_base(bases,joueur,discard)
#            if effet[-1][0] == "loot":
#                loot(hand,discard,joueur,deck)
            effet.pop()
        if allies >= 0:
            couleur[allies][1] += 1
            if couleur[allies][1] == 1:
                if len(hand[joueur][0]) > 7:
                    for ef_c in hand[joueur][0][7]:
                        couleur[allies][0].append(ef_c)
        point_tour[0] += p
        credits_tot += c
        if hand[joueur][0][9][0] == "base":
            print(hand[joueur][0])
            print(hand[joueur][0][9][1])
            bases[joueur].append([hand[joueur][0],hand[joueur][0][9][1]])
            print(">>>>>>>>>>>>>>>>",bases[joueur],"<<<<<<<<<<<<<<<<<<<<<<")
        else:
            played.append(hand[joueur][0])
        hand[joueur].pop(0)
    while effet_scrap != []:
        img = mp.imread(effet_scrap[-1][2] + " scrap.jpg")
        plt.imshow(img)
        plt.axis('off')
        plt.show()
        print("=======>",effet_scrap[-1][2],"peut se scrap pour",effet_scrap[-1][0])
        affiche,affiche_base = [],[]
        for carte in shop:
            affiche.append(carte[5])
        for carte in bases[1-joueur]:
            affiche_base.append(carte[0][5])
        print("------< le shop >------- : ",affiche)
        print("------< les bases >------- : ",affiche_base)
        print("------< les scrores >------- : ",points)
        print("=======> Voulez vous le scrap ? ( 1 pour oui, 0 pour non )")
        k = int(input())
        if  k == 1:
            for defausse in effet_scrap[-1][0]:
                if defausse[0] == "draw":
                    draw(1,joueur,deck,hand,discard)
                if defausse[0] == "heal":
                    heal(joueur,defausse[1],points)
                if defausse[0] == "scrap":
                    print("############################################################### Scrap ###############################################################")
                    scrap_player(joueur,discard)
                if defausse[0] == "switch":
                    futur_switch += 1
                if defausse[0] == "discard":
                    print("############################################################### Discard ###############################################################")
                    discard_player(hand,discard,joueur)
                if defausse[0] == "damages":
                    more_damages(defausse[1],points,point_tour,joueur)
                if defausse[0] == "more_gold":
                    credits_tot += defausse[1]
                if defausse[0] == "destroy":
                    destroy_base(bases,joueur,discard)
            featch(played,effet_scrap[-1][1],bases,joueur)
        effet_scrap.pop()
    param = [credits_tot,couleur,point_tour,futur_switch,played]
    if hand[joueur] != []:
        boucle(joueur,deck,hand,discard,cartes_A,points,shop,ide_prospecteur,param)
    else:
        return param


def play_turn_player(joueur,deck,hand,discard,cartes_A,points,shop,ide_prospecteur):
    credits_tot = 0
    couleur = [[[],0],[[],0],[[],0],[[],0]]
    point_tour = [0]
    played = []
    futur_switch = 0
    param = [credits_tot,couleur,point_tour,futur_switch,played]
    param = boucle(joueur,deck,hand,discard,cartes_A,points,shop,ide_prospecteur,param)
    [credits_tot,couleur,point_tour,futur_switch,played] = param
#     while bases[joueur] != []:
#         hand[joueur].append(bases[joueur][0][0])
#         bases[joueur].pop(0)
#     while hand[joueur] != []:
#         [ide,a,p,c,e,n] = hand[joueur][0][:6]
#         effet = []
#         if len(hand[joueur][0]) > 6:
#             for ef in hand[joueur][0][6]:
#                 effet.append(ef)
#         allies = empreinte_couleur(e)
#         if allies >= 0 and couleur[allies][1] > 0:
#             if couleur[allies][1] ==1:
#                 for ef_c in couleur[allies][0]:
#                     effet.append(ef_c)
#             else:
#                 if len(hand[joueur][0]) > 7:
#                     for ef_c in hand[joueur][0][7]:
#                         effet.append(ef_c)
#         while effet != []:
#             if effet[-1][0] == "draw":
#                 draw(1,joueur,deck,hand,discard)
#             if effet[-1][0] == "heal":
#                 heal(joueur,effet[0][1],points)
#             if effet[-1][0] == "scrap":
#                 print("############################################################### Scrap ###############################################################")
#                 scrap_player(joueur,discard)
#             if effet[-1][0] == "switch":
#                 futur_switch += 1
#             if effet[-1][0] == "discard":
#                 print("############################################################### Discard ###############################################################")
#                 discard_player(hand,discard,joueur)
#             if effet[-1][0] == "damages":
#                 more_damages(effet[-1][1],points,point_tour,joueur)
#             if effet[-1][0] == "more_gold":
#                 credits_tot += effet[-1][1]
#             if effet[-1][0] == "destroy":
#                 destroy_base(bases,joueur,discard)
# #            if effet[-1][0] == "loot":
# #                loot(hand,discard,joueur,deck)
#             effet.pop()
#         if allies >= 0:
#             couleur[allies][1] += 1
#             if couleur[allies][1] == 1:
#                 if len(hand[joueur][0]) > 7:
#                     for ef_c in hand[joueur][0][7]:
#                         couleur[allies][0].append(ef_c)
#         point_tour[0] += p
#         credits_tot += c
#         if hand[joueur][0][9][0] == "base":
#             print(hand[joueur][0])
#             print(hand[joueur][0][9][1])
#             bases[joueur].append([hand[joueur][0],hand[joueur][0][9][1]])
#             print(">>>>>>>>>>>>>>>>",bases[joueur],"<<<<<<<<<<<<<<<<<<<<<<")
#         else:
#             played.append(hand[joueur][0])
#         hand[joueur].pop(0)
    repart_points(points,joueur,bases,discard,point_tour[0])
    for t in range(futur_switch):
        print("############################################################### Switch ###############################################################")
        switch_player(shop,cartes_A,credits_tot)
    buy_player(joueur,credits_tot,cartes_A,discard,shop,ide_prospecteur)
    print()
    print("----------- Vous avez jouez :",played," -----------")
    print("----------- Et vos bases sont :",bases[joueur],"-----------")
    print(" ( Le nombre de dégats fait durant ce tour est :",point_tour[0],")")
    retour[1-joueur].append(points[1-joueur])
    print()
    discard[joueur] += played
    affichage(discard[joueur])
    draw(5,joueur,deck,hand,discard)


joueur = rdm.randint(0,1)
draw(3,joueur,deck,hand,discard)
draw(5,1-joueur,deck,hand,discard)


def game_inter_2players(F,deck,hand,discard,cartes_A,points,shop,ide_prospecteur,joueur):
    while points[0] > 0 and points[1] > 0:
        if joueur == 0:
            print()
            print("##########################################################     C'est au joueur 0 de jouer     ##########################################################")
            play_turn_player(joueur,deck,hand,discard,cartes_A,points,shop,ide_prospecteur)
        else:
            print()
            print("##########################################################     C'est au joueur 1 de jouer     ##########################################################")
            play_turn_player(joueur,deck,hand,discard,cartes_A,points,shop,ide_prospecteur)
        print()
        print("################################################################# Les scores :",points,"################################################################")
        joueur = 1 - joueur
    if points[0] >= 0:
        print("=======> Le joueur 0 a gagné !")
    else:
        print("=======> Le joueur 1 a gagné !")

game_inter_2players(5,deck,hand,discard,cartes_A,points,shop,ide_prospecteur,joueur)
plt.plot(retour[0])
plt.plot(retour[1])
plt.legend(["Joueur 0","Joueur 1"])
plt.show
